@import XCTest;

@interface XCTestCase (RunLoop)

// Asynchronous Unit Tests :
// Runs the runloop until timeoutSecs seconds elapse or the completed flag is set.
- (BOOL)waitForCompletion:(NSTimeInterval)timeoutSecs flag:(BOOL*)completed;
@end
