@import XCTest;

#import "CollectionsAdditions.h"

@interface NSArrayAdditionsTests : XCTestCase
@end

@implementation NSArrayAdditionsTests

- (void) testFirstObjectWithValue
{
    NSArray * testdata = (@[
                          @{@"key" : @"v", @"id": @1},
                          @{@"key" : @"a", @"id": @2},
                          @{@"key" : @"l", @"id": @3},
                          @{@"key" : @"u", @"id": @4},
                          @{@"key" : @"e", @"id": @5},
                          ]);
    
    XCTAssertEqualObjects([[testdata firstObjectWithValue:@"a" forKeyPath:@"key"] objectForKey:@"id"], @2);
}

- (void) testFilteredArrayWithValueForKey
{
    NSArray * testdata = (@[
                          @{@"key" : @"b", @"id": @1},
                          @{@"key" : @"a", @"id": @2},
                          @{@"key" : @"b", @"id": @3},
                          @{@"key" : @"a", @"id": @4},
                          @{@"key" : @"b", @"id": @5},
                          ]);
    
    NSArray * expectedResult = (@[
                                @{@"key" : @"b", @"id": @1},
                                @{@"key" : @"b", @"id": @3},
                                @{@"key" : @"b", @"id": @5},
                                ]);
    XCTAssertEqualObjects([testdata filteredArrayWithValue:@"b" forKeyPath:@"key"], expectedResult);
}

@end

@interface NSSetAdditionsTests : XCTestCase
@end

@implementation NSSetAdditionsTests

- (void) testAnyObjectWithValue
{
    NSSet * testdata = [NSSet setWithArray:@[
                        @{@"key" : @"v", @"id": @1},
                        @{@"key" : @"a", @"id": @2},
                        @{@"key" : @"l", @"id": @3},
                        @{@"key" : @"u", @"id": @4},
                        @{@"key" : @"e", @"id": @5},
                        ]];
    
    XCTAssertEqualObjects([[testdata anyObjectWithValue:@"a" forKeyPath:@"key"] objectForKey:@"id"], @2);
}

- (void) testFilteredSetWithValueForKey
{
    NSSet * testdata = [NSSet setWithArray:@[
                        @{@"key" : @"b", @"id": @1},
                        @{@"key" : @"a", @"id": @2},
                        @{@"key" : @"b", @"id": @3},
                        @{@"key" : @"a", @"id": @4},
                        @{@"key" : @"b", @"id": @5},
                        ]];
    
    NSSet * expectedResult = [NSSet setWithArray:@[
                              @{@"key" : @"b", @"id": @1},
                              @{@"key" : @"b", @"id": @3},
                              @{@"key" : @"b", @"id": @5},
                              ]];
    XCTAssertEqualObjects([testdata filteredSetWithValue:@"b" forKeyPath:@"key"], expectedResult);
}

@end
