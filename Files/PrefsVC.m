#import "PrefsVC.h"
#import "BicycletteCity+Update.h"
#import "CitiesController.h"
#import "NSProcessInfo+HardwareMachine.h"
#import "Style.h"
#import "UIBarButtonItem+BICMargins.h"
#import "GeofencesMonitor.h"
#import "StationsStatusSummary.h"

@implementation PrefsVC
{
    CitiesController * _controller;

    IBOutlet UITableViewCell * _geofencesCell;
    IBOutlet UISwitch * _geofencesSwitch;
    
    IBOutlet UITableViewCell * _updateStationsCell;
    IBOutlet UIActivityIndicatorView * _updateIndicator;

    IBOutlet UITableViewCell * _emailSupportCell;
    IBOutlet UITableViewCell * _rateOnAppStoreCell;

    IBOutlet UITableViewCell * _dataCreditsCell;

    IBOutlet id _creditsSection;
}

// Life cycle

+ (UIViewController*) prefsVCWithController:(CitiesController *)controller
{
    PrefsVC * prefsVC = [[UIStoryboard storyboardWithName:@"PrefsVC" bundle:nil] instantiateInitialViewController];
    prefsVC.controller = controller;
    UINavigationController * navC = [[UINavigationController alloc] initWithRootViewController:prefsVC];
    navC.navigationBar.barTintColor = kBicycletteBlue;
    navC.navigationBar.tintColor = [UIColor whiteColor];
    navC.navigationBar.barStyle = UIBarStyleBlack;
    navC.navigationBarHidden = NO;
    navC.toolbarHidden = YES;
    navC.modalPresentationStyle = UIModalPresentationFormSheet;
    return navC;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    self.title = [NSString stringWithFormat:@"%@ %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissPrefsVC)];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityDataUpdated:) name:BicycletteCityNotifications.updateBegan object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityDataUpdated:) name:BicycletteCityNotifications.updateGotNewData object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityDataUpdated:) name:BicycletteCityNotifications.updateSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityDataUpdated:) name:BicycletteCityNotifications.updateFailed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(monitoringEnabledDidChange) name:GeofencesMonitorNotifications.monitoringEnabledDidChange object:nil];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_controller removeObserver:self forKeyPath:@"currentCity" context:__FILE__];
}

// Data
- (void) setController:(CitiesController *)controller_
{
    [_controller removeObserver:self forKeyPath:@"currentCity" context:__FILE__];
    _controller = controller_;
    [_controller addObserver:self forKeyPath:@"currentCity" options:NSKeyValueObservingOptionInitial context:__FILE__];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == __FILE__) {
        [self updateUpdateLabel];
        [self updateCreditsSection];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void) monitoringEnabledDidChange
{
    [self updateGeofencesSection];
    [self.tableView reloadData];
}

- (void) appDidBecomeActive
{
    [self updateGeofencesSection];
    [self.tableView reloadData];
}

// Autorotation support
- (BOOL) shouldAutorotate { return YES; }
- (NSUInteger)supportedInterfaceOrientations { return UIInterfaceOrientationMaskAll; }

// View Cycle
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // Geofences
    [self updateGeofencesSection];

    // Updates
    [self updateUpdateLabel];

    // Support
    _emailSupportCell.textLabel.text = [self supportEmailAddress];
    _emailSupportCell.textLabel.textColor = kBicycletteBlue;
    
    // Credits
    [self updateCreditsSection];
}

- (void) updateGeofencesSection
{
    _geofencesSwitch.onTintColor = kBicycletteBlue;
    if(! [CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]){
        _geofencesCell.detailTextLabel.text = NSLocalizedString(@"prefs.geofences.unavailable", nil);
        _geofencesSwitch.on = NO;
    } else if( (![_controller.fencesMonitor hasLocationAuthorization] && [_controller.fencesMonitor hasRequestedLocationAuthorization])
              || ( ![StationsStatusSummary hasNotificationAuthorization] && [StationsStatusSummary hasRequestedNotificationAuthorization])) {
        NSMutableAttributedString * detailText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"prefs.geofences.unauthorized.1", nil) attributes:nil];
        if([[NSProcessInfo processInfo] respondsToSelector:@selector(isOperatingSystemAtLeastVersion:)] && [[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){8,0,0}]) {
            [detailText.mutableString appendString:@"\n"];
            [detailText appendAttributedString:[[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"prefs.geofences.unauthorized.2", nil)
                                                                                      attributes:@{NSForegroundColorAttributeName:kBicycletteBlue}]];
        }
        _geofencesCell.detailTextLabel.attributedText = detailText;
        _geofencesSwitch.on = NO;
    } else {
        _geofencesCell.detailTextLabel.text = nil;
        _geofencesSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"RegionMonitoring.Enabled"] && [_controller.fencesMonitor hasLocationAuthorization] && [_controller.fencesMonitor hasRequestedLocationAuthorization];
    }
}

- (void) updateCreditsSection
{
    NSString * message;
    if(_controller.currentCity) {
        message = [NSString stringWithFormat:NSLocalizedString(@"prefs.credits.footer",nil),
                   _controller.currentCity.serviceName,
                   _controller.currentCity.serviceName];
    }
    if([_creditsSection respondsToSelector:NSSelectorFromString(@"setFooterTitle:")]) {
        [_creditsSection setValue:message forKey:@"footerTitle"];
    }
    _dataCreditsCell.textLabel.textColor = kBicycletteBlue;
}

// UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    if(indexPath.section==0) {
        cell = _geofencesCell;
    } else if(indexPath.section==1) {
        cell = _updateStationsCell;
    }

    if(cell) {
        // Hacky, at least it's short.
        // This tableviewcontroller is using stock UITableViewCell styles,
        // statically designed in a storyboard with autolayout support.
        // Why the hell do I need to do anything to have proper cell heights ?
        [cell layoutIfNeeded];
        CGFloat height;
        if([cell.detailTextLabel.text length]) {
            height = CGRectGetMaxY(cell.detailTextLabel.frame) - CGRectGetMinY(cell.textLabel.frame) + 16;
        } else {
            height = CGRectGetHeight(cell.textLabel.frame) + 16;
        }
        return MAX(height, cell.accessoryView.bounds.size.height + 16);
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

// UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell==_geofencesCell) {
        if( ![_controller.fencesMonitor hasLocationAuthorization] || ![StationsStatusSummary hasNotificationAuthorization]) {
            [self openPermissionSettings];
        }
    } else if(cell==_updateStationsCell) {
        [self updateStationsList];
    } else if(cell==_emailSupportCell) {
        [self openEmailSupport];
    } else if(cell==_rateOnAppStoreCell) {
        [self rate:nil];
    } else if(cell==_dataCreditsCell) {
        [self openCredits];
    }
}

/****************************************************************************/
#pragma mark Actions

- (IBAction)dismissPrefsVC
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Geofences

- (IBAction) switchRegionMonitoring:(UISwitch*)sender
{
    if(![_controller.fencesMonitor hasLocationAuthorization]) {
        [_controller.fencesMonitor requestLocationAuthorization];
    }
    if(![StationsStatusSummary hasNotificationAuthorization]) {
        [StationsStatusSummary requestNotificationAuthorization];
    }
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:@"RegionMonitoring.Enabled"];
}

- (IBAction) openPermissionSettings
{
    if([[NSProcessInfo processInfo] respondsToSelector:@selector(isOperatingSystemAtLeastVersion:)] && [[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){8,0,0}]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

// City updates

- (IBAction)updateStationsList
{
    [_controller.currentCity updateWithCompletionBlock:nil];
}

- (void) updateUpdateLabel
{
    if(_controller.currentCity) {
        _updateStationsCell.userInteractionEnabled = YES;
        _updateStationsCell.textLabel.enabled = YES;
        _updateStationsCell.detailTextLabel.enabled = YES;
        NSUInteger count = [_controller.currentCity.mainContext countForFetchRequest:[[NSFetchRequest alloc] initWithEntityName:[Station entityName]] error:NULL];
        _updateStationsCell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"prefs.updates.stationsCountOfType.%d%@", nil),
                                                    (int)count,
                                                    _controller.currentCity.title];
    } else {
        _updateStationsCell.userInteractionEnabled = NO;
        _updateStationsCell.textLabel.enabled = NO;
        _updateStationsCell.detailTextLabel.enabled = NO;
        _updateStationsCell.detailTextLabel.text = nil;
    }
}

- (void)cityDataUpdated:(NSNotification*)note
{
    if(note.object!=_controller.currentCity) {
        return;
    }
    
    if([note.name isEqualToString:BicycletteCityNotifications.updateBegan]) {
        // Began
        _updateStationsCell.userInteractionEnabled = NO;
        _updateStationsCell.textLabel.enabled = NO;
        _updateStationsCell.detailTextLabel.enabled = NO;
        _updateStationsCell.detailTextLabel.text = NSLocalizedString(@"update.status.fetching", nil);
        [_updateIndicator startAnimating];
    } else if([note.name isEqualToString:BicycletteCityNotifications.updateGotNewData]) {
        // Parsing
        _updateStationsCell.detailTextLabel.text = NSLocalizedString(@"update.status.parsing", nil);
    } else if([note.name isEqualToString:BicycletteCityNotifications.updateSucceeded]) {
        // Success
        _updateStationsCell.userInteractionEnabled = YES;
        _updateStationsCell.textLabel.enabled = YES;
        _updateStationsCell.detailTextLabel.enabled = YES;
        [_updateIndicator stopAnimating];
        [self updateUpdateLabel];
        NSArray * saveErrors = note.userInfo[BicycletteCityNotifications.keys.saveErrors];
        if ([saveErrors count]!=0
            && self.navigationController.visibleViewController==self) { //Only display error if visible
            NSString * message = [NSString stringWithFormat:@"%@\n%@.",
                       NSLocalizedString(@"update.status.completedWithErrors", nil),
                       [[saveErrors valueForKey:@"localizedDescription"] componentsJoinedByString:@",\n"]];
            NSString * title = NSLocalizedString(@"update.status.completed", nil);
            [[[UIAlertView alloc] initWithTitle:title
                                        message:message
                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    } else if([note.name isEqualToString:BicycletteCityNotifications.updateFailed]) {
        // Failure
        _updateStationsCell.userInteractionEnabled = YES;
        _updateStationsCell.textLabel.enabled = YES;
        _updateStationsCell.detailTextLabel.enabled = YES;
        [_updateIndicator stopAnimating];
        [self updateUpdateLabel];

        if(self.navigationController.visibleViewController==self) { //Only display error if visible
            NSError * error = note.userInfo[BicycletteCityNotifications.keys.failureError];
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"update.status.failed",nil)
                                        message:[error localizedDescription]
                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
}

// Support

- (NSString *) supportEmailAddress
{
    return @"contact@bicyclette-app.com";
}

- (IBAction) openEmailSupport
{
    NSString * emailAddress = [self supportEmailAddress];
    
    NSString * techSummary = [NSString stringWithFormat:NSLocalizedString(@"support.email.template.%@%@%@%@", nil),
                              [NSString stringWithFormat:@"%@ (%@)",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleVersionKey]],
                              [NSString stringWithFormat:@"%@ (%@)",[[UIDevice currentDevice] model], [[NSProcessInfo processInfo] hardwareMachine]],
                              [NSString stringWithFormat:@"%@ (%@)",[[UIDevice currentDevice] systemName],[[UIDevice currentDevice] systemVersion]],
                              _controller.currentCity.cityName?:@""
                              ];
    techSummary = [techSummary stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString * emailLink = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", emailAddress, [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey], techSummary];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:emailLink]];
}

- (NSURL*) appURLOnStore
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", @"546171712"]];
}

- (IBAction) rate:(id)sender
{
    [[UIApplication sharedApplication] openURL:[self appURLOnStore]];
}

// Credits

- (NSURL*) creditsURL
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.bicyclette-app.com/data.html"]];
}

- (IBAction) openCredits
{
    [[UIApplication sharedApplication] openURL:[self creditsURL]];
}

@end
