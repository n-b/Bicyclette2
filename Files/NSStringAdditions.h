@import Foundation;

@interface NSString (NSStringAdditions)

- (NSString*) stringByDeletingPrefix:(NSString*) prefix;
- (NSString*) stringByTrimmingZeros;
- (NSString*) stringByTrimmingWhitespace;
- (NSString*) capitalizedStringWithCurrentLocale;

@end
