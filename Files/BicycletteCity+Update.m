#import "BicycletteCity+Update.h"

#import <objc/runtime.h>

#import "DebugLog.h"
#import "NSObject+KVCMapping.h"
#import "CollectionsAdditions.h"
#import "NSError+MultipleErrorsCombined.h"
#import "_StationParse.h"
#import "Station+Update.h"

@implementation BicycletteCity (Update)

#pragma mark Data Updates

- (NSArray *) updateURLStrings { return @[self.serviceInfo[@"update_url"]]; }
- (NSString *) detailsURLStringForStation:(Station*)station { return [self.serviceInfo[@"station_details_url"] stringByAppendingString:station.number]; }

- (KVCEntityMapping*) KVCMapping
{
    KVCEntityMapping * mapping = objc_getAssociatedObject(self, _cmd);
    if(nil==mapping) {
        mapping = [[KVCEntityMapping alloc] initWithMappingDictionary:[self KVCMappingDictionary]
                                                           primaryKey:Station.attributes.number
                                                           entityName:[Station entityName]];
        objc_setAssociatedObject(self, _cmd, mapping, OBJC_ASSOCIATION_RETAIN);
    }
    return mapping;
}

- (NSDictionary*) KVCMappingDictionary
{
    return self.serviceInfo[@"KVCMapping"];
}

- (Class) stationStatusParsingClass
{
    return nil;
}

- (BOOL) canUpdateIndividualStations
{
    return [self stationStatusParsingClass] != nil;
}

- (BOOL) canShowFreeSlots
{
    return ([[[self KVCMapping] mappingsTo:Station.attributes.status_free] count]!=0
            || [[[self KVCMapping] mappingsTo:Station.attributes.status_total] count]!=0);
}

- (NSArray *)cachedStationsStatus
{
    return objc_getAssociatedObject(self, @selector(cachedStationsStatus));
}

- (void) cacheStationsStatus
{
    NSFetchRequest * cachedStatusRequest = [NSFetchRequest fetchRequestWithEntityName:[Station entityName]];
    cachedStatusRequest.resultType = NSDictionaryResultType;
    cachedStatusRequest.propertiesToFetch = @[Station.attributes.open,
                                              Station.attributes.status_available,
                                              Station.attributes.status_free,
                                              Station.attributes.status_date,
                                              Station.attributes.latitude,
                                              Station.attributes.longitude,
                                              Station.attributes.starred
                                              ];
    NSError * error;
    NSArray * status = [self.mainContext executeFetchRequest:cachedStatusRequest error:&error];
    NSAssert(error==nil, @"Should not fail to cache stations status %@", error);
    
    objc_setAssociatedObject(self, @selector(cachedStationsStatus), status, OBJC_ASSOCIATION_RETAIN);
}

- (void) updateWithCompletionBlock:(void (^)(NSError *))completion_
{
    if(self.updater==nil)
    {
        self.updater = [[DataUpdater alloc] initWithURLStrings:[self updateURLStrings] delegate:self];
        _updateCompletionBlock = [completion_ copy];
        [[NSNotificationCenter defaultCenter] postNotificationName:BicycletteCityNotifications.updateBegan object:self];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(becomeStale) object:nil];
    }
    else if(completion_)
            completion_(nil);
}

- (void) updater:(DataUpdater *)updater didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BicycletteCityNotifications.updateFailed object:self userInfo:@{BicycletteCityNotifications.keys.failureError : error}];
    if(_updateCompletionBlock)
        _updateCompletionBlock(error);
    _updateCompletionBlock = nil;
    self.updater = nil;
}

- (void) updaterDidFinishWithNoNewData:(DataUpdater *)updater
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BicycletteCityNotifications.updateSucceeded object:self userInfo:@{BicycletteCityNotifications.keys.dataChanged : @(NO)}];
    if(_updateCompletionBlock)
        _updateCompletionBlock(nil);
    _updateCompletionBlock = nil;
    self.updater = nil;
}

- (void) updater:(DataUpdater*)updater finishedWithNewDataChunks:(NSDictionary*)datas
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BicycletteCityNotifications.updateGotNewData object:self];
    
    __block NSError * validationErrors;
    [self performUpdates:^(NSManagedObjectContext *updateContext) {
        _parsing_context = updateContext;

        // Create instances cache
        KVCInstancesCache * instancesCache = [[KVCInstancesCache alloc] initWithContext:_parsing_context entityMapping:[self KVCMapping]];
        _parsing_cache = [[KVCEntitiesCache alloc] initWithInstanceCaches:@[instancesCache]];
        
        // Parsing
        for (NSString * urlString in datas) {
            _parsing_urlString = urlString;
            [self parseData:datas[urlString]];
            _parsing_urlString = nil;
        }
        _parsing_context = nil;
        _parsing_cache = nil;

        // Delete Old Stations
        for (Station * oldStation in [instancesCache unaccessedInstances]) {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"BicycletteLogParsingDetails"])
                DebugLog(@"Note : old station deleted after update : %@", oldStation);
            [updateContext deleteObject:oldStation];
        }
        
    } saveCompletion:^(NSNotification *contextDidSaveNotification) {
        NSMutableDictionary * userInfo = [@{BicycletteCityNotifications.keys.dataChanged : @(YES)} mutableCopy];
        if (validationErrors)
            userInfo[BicycletteCityNotifications.keys.saveErrors] = [validationErrors underlyingErrors];
        [[NSNotificationCenter defaultCenter] postNotificationName:BicycletteCityNotifications.updateSucceeded object:self
                                                          userInfo:userInfo];
        
        [self cacheStationsStatus];
        
        if(![self canUpdateIndividualStations]) {
            [self performSelector:@selector(becomeStale) withObject:nil afterDelay:[[NSUserDefaults standardUserDefaults] doubleForKey:@"StationStatusStalenessInterval"]];
        }

        if(_updateCompletionBlock)
            _updateCompletionBlock(nil);
        _updateCompletionBlock = nil;
    }];
    self.updater = nil;
}

- (void) becomeStale
{
    [[NSNotificationCenter defaultCenter] postNotificationName:StationNotifications.didBecomStale object:nil];
}

- (void) setStation:(Station*)station attributes:(NSDictionary*)stationAttributes
{
    [station kvc_setValues:stationAttributes withEntityMapping:[self KVCMapping] options:nil]; // Yay!
    [self fixupParsingInStation:station attributes:stationAttributes];
}

- (void) insertStationWithAttributes:(NSDictionary*)stationAttributes
{
    Station * station = (Station*)[_parsing_context kvc_importObject:stationAttributes withEntityMapping:[self KVCMapping] // Double Yay!
                                                             options:@{KVCEntitiesCacheOption:_parsing_cache, KVCCreateObjectOption: @YES}];
    [self fixupParsingInStation:station attributes:stationAttributes];
}

- (void) fixupParsingInStation:(Station*)station attributes:(NSDictionary*)stationAttributes
{
    BOOL logParsingDetails = [[NSUserDefaults standardUserDefaults] boolForKey:@"BicycletteLogParsingDetails"];
    //
    // Set patches
    NSDictionary * patchs = [self patches][station.number];
    BOOL hasDataPatches = patchs && ![[[patchs allKeys] arrayByRemovingObjectsInArray:[[self KVCMapping] allKeys]] isEqualToArray:[patchs allKeys]];
    if(hasDataPatches)
    {
        if(logParsingDetails)
            DebugLog(@"Note : Used hardcoded fixes %@. Fixes : %@.",stationAttributes, patchs);
        [station kvc_setValues:patchs withEntityMapping:[self KVCMapping] options:nil]; // Yay! again
    }
    
    //
    // Build missing status, if needed
    NSAssert([[[self KVCMapping] mappingsTo:Station.attributes.status_available] count]>0,nil);
    if([[self KVCMapping] extractValueFor:Station.attributes.status_available fromValues:stationAttributes])
    {
        if([[[self KVCMapping] mappingsTo:Station.attributes.status_total] count]==0
           && [[[self KVCMapping] mappingsTo:Station.attributes.status_free] count]!=0)
        {
            // "Total" is not in data but "Free" is
            station.status_total = station.status_free + station.status_available;
        }
        else if ([[[self KVCMapping] mappingsTo:Station.attributes.status_free] count]==0
                 && [[[self KVCMapping] mappingsTo:Station.attributes.status_total] count]!=0)
        {
            // "Free" is not in data but "Total" is
            station.status_free = station.status_total - station.status_available;
        }
        
        // Set Date to now
        station.status_date = [NSDate date];
    }
}


@end
