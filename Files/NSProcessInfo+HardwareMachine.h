@import Foundation;

@interface NSProcessInfo (HardwareMachine)
- (NSString*) hardwareMachine;
@end
