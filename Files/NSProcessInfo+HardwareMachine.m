#import "NSProcessInfo+HardwareMachine.h"
#include <sys/sysctl.h>

@implementation NSProcessInfo (HardwareMachine)

- (NSString*) hardwareMachine
{
    size_t len = 0;
    sysctlbyname("hw.machine", NULL, &len, NULL, 0);
    if (len) {
        char *model = malloc(len*sizeof(char));
        sysctlbyname("hw.machine", model, &len, NULL, 0);
        NSString * result = [[NSString alloc] initWithCString:model encoding:NSASCIIStringEncoding];
        free(model);
        return result;
    }
    return nil;
}

@end
