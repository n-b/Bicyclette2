#import "Station.h"
#import "BicycletteCity.h"

@implementation Station
{
    CLLocationCoordinate2D _coordinate;
    BOOL _coordinateCached;
    CLLocation * _location;
}

- (void)dealloc
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

// Meta info

+ (NSString*) entityName { return @"Station"; }

+  (StationAttributes) attributes
{
    return (StationAttributes){
        .address = @"address",
        .bonus = @"bonus",
        .color = @"color",
        .fullAddress = @"fullAddress",
        .latitude = @"latitude",
        .longitude = @"longitude",
        .name = @"name",
        .number = @"number",
        .open = @"open",
        .starred = @"starred",
        .status_available = @"status_available",
        .status_date = @"status_date",
        .status_free = @"status_free",
        .status_ticket = @"status_ticket",
        .status_total = @"status_total",
    };
}

// Attributes

@dynamic number, name, address, fullAddress;
@dynamic latitude, longitude;
@dynamic open, bonus;
@dynamic starred;
@dynamic status_date, status_available, status_free, status_total, status_ticket;

// Validation

- (BOOL)validateForInsert:(NSError **)error
{
    BOOL propertiesValid = [super validateForInsert:error];
    // could stop here if invalid
    BOOL consistencyValid = [self validateConsistency:error];
    return (propertiesValid && consistencyValid);
}

- (BOOL)validateForUpdate:(NSError **)error
{
    BOOL propertiesValid = [super validateForUpdate:error];
    // could stop here if invalid
    BOOL consistencyValid = [self validateConsistency:error];
    return (propertiesValid && consistencyValid);
}

- (BOOL)validateConsistency:(NSError **)error
{
    CLCircularRegion * validRegion = self.city.validRegion;
    if(validRegion.radius==0) {
        return YES;
    }
    
    if([validRegion containsCoordinate:self.location.coordinate]) {
        return YES;
    }
    
    if (error != NULL) {
        NSError * limitsError = [NSError errorWithDomain: NSCocoaErrorDomain
                                                    code: NSManagedObjectValidationError
                                                userInfo:@{
                                                           NSValidationObjectErrorKey : self,
                                                           NSValidationKeyErrorKey : @"location",
                                                           NSValidationValueErrorKey : self.location,
                                                           NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Station %@ (%@)", 0), self.number, self.name],
                                                           NSLocalizedFailureReasonErrorKey : [NSString stringWithFormat:NSLocalizedString(@"Invalid coordinates (%f, %f)", 0),self.location.coordinate.latitude, self.location.coordinate.longitude],
                                                           }];
        *error = [NSError errorFromOriginalError:*error error:limitsError];
    }
    return NO;
}

// MKAnnotation, Locatable

- (NSString *) title
{
    return [[self city] titleForStation:self];
}

- (NSString *) subtitle
{
    return [[self city] subtitleForStation:self];
}

- (CLLocationCoordinate2D) coordinate
{
    if(!_coordinateCached) {
        _coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
        _coordinateCached = YES;
    }
    return _coordinate;
}

- (CLLocation*) location
{
    if(!_location){
        _location = [[CLLocation alloc] initWithLatitude:self.latitude longitude:self.longitude];
    }
    return _location;
}

- (void)didSave
{
    [super didSave];
    _location = nil;
    _coordinateCached = NO;
}

@end
