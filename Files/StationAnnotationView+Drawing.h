#import "Style.h"
#import "StationAnnotationView.h"


@interface StationAnnotationView (Drawing)
+ (CGImageRef)sharedImageWithMode:(StationAnnotationMode)mode
                  backgroundColor:(UIColor*)backgroundColor
                          starred:(BOOL)starred
                            value:(NSString*)text;
@end
