#import "FlatJSONListCity.h"
#import "DataUpdater.h"
#import "NSValueTransformer+TransformerKit.h"

@interface BCycleCity : FlatJSONListCity
@end

@implementation BCycleCity

+ (void)initialize
{
    [NSValueTransformer registerValueTransformerWithName:@"BCycleStatus" transformedValueClass:[NSNumber class]
                      returningTransformedValueWithBlock:^NSNumber*(NSString* value) {
                          NSArray * OpenStatus = @[@"Active", @"PartialService", @"SpecialEvent"];
                          // ClosedStatus = @[@"Unavailable", @"ComingSoon"];
                          return @([OpenStatus containsObject:value]);
                      }];
}

- (NSArray *)updateURLStrings
{
    return @[[NSString stringWithFormat:@"https://publicapi.bcycle.com/api/1.0/ListProgramKiosks/%@", self.serviceInfo[@"bcycle_program_id"]]];
}

- (NSURLRequest*) updater:(DataUpdater*)updater requestForURLString:(NSString *)urlString
{
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request addValue:self.accountInfo[@"apikey"] forHTTPHeaderField:@"ApiKey"];
    return request;
}

- (NSDictionary *)KVCMappingDictionary
{
    return @{
             @"Id": @"number",
             @"Location.Longitude": @"longitude",
             @"Location.Latitude": @"latitude",
             @"Name": @"name",
             @"Address": @"address",
             @"BikesAvailable": @"status_available",
             @"DocksAvailable": @"status_free",
             @"TotalDocks": @"status_total",
             @"Status": @"BCycleStatus:open"
             };
}

@end
