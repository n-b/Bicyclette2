
#import "AltaBikeShareCity.h"

@interface BayAreaBikeShareCity : AltaBikeShareCity
@end

@implementation BayAreaBikeShareCity

// Mapping
- (NSArray *)updateURLStrings
{
    return @[@"http://www.bayareabikeshare.com/stations/json/"];
}

@end


// Separate SF and Silicon Valley

static NSString * SANFRANCISCO = @"San Francisco";
@interface SanFranciscoBikeShareCity : BayAreaBikeShareCity
@end
@implementation SanFranciscoBikeShareCity
- (void) insertStationWithAttributes:(NSDictionary*)stationAttributes
{
    if([stationAttributes[@"city"] isEqualToString:SANFRANCISCO]) {
        [super insertStationWithAttributes:stationAttributes];
    }
}
@end

@interface SiliconValleyBikeShareCity : BayAreaBikeShareCity
@end
@implementation SiliconValleyBikeShareCity
- (void) insertStationWithAttributes:(NSDictionary*)stationAttributes
{
    if( ! [stationAttributes[@"city"] isEqualToString:SANFRANCISCO]) {
        [super insertStationWithAttributes:stationAttributes];
    }
}
@end