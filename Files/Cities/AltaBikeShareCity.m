
#import "AltaBikeShareCity.h"
#import "NSValueTransformer+TransformerKit.h"

@implementation AltaBikeShareCity

+ (void)initialize
{
    [NSValueTransformer registerValueTransformerWithName:@"AltaBikeShareStatus" transformedValueClass:[NSNumber class]
                      returningTransformedValueWithBlock:^NSNumber*(NSNumber* value) {
                          return @([value isKindOfClass:[NSNumber class]] && [value isEqualToNumber:@1]);
                      }];
}

- (NSString*) keyPathToStationsLists
{
    return @"stationBeanList";
}

- (NSDictionary *)KVCMappingDictionary
{
    return @{@"stAddress1": @"address",
             @"latitude": @"latitude",
             @"availableDocks": @"status_free",
             @"stationName": @"name",
             @"id": @"number",
             @"longitude": @"longitude",
             @"availableBikes": @"status_available",
             @"statusKey": @"AltaBikeShareStatus:open",
             };
}

@end
