#import "FlatJSONListCity.h"
#import "NSValueTransformer+TransformerKit.h"

@interface PubliBikeCity : FlatJSONListCity
@end

@implementation PubliBikeCity

+ (void)initialize
{
    [NSValueTransformer registerValueTransformerWithName:@"PubliBikeCity_StatusFree_Transformer" transformedValueClass:[NSNumber class]
                      returningTransformedValueWithBlock:^NSNumber*(NSArray* value) {
                          if([value isKindOfClass:[NSArray class]]) {
                              return [value valueForKeyPath:@"@sum.holdersfree"];
                          }
                          return @0;
                      }];
    [NSValueTransformer registerValueTransformerWithName:@"PubliBikeCity_StatusTotal_Transformer" transformedValueClass:[NSNumber class]
                      returningTransformedValueWithBlock:^NSNumber*(NSArray* value) {
                          if([value isKindOfClass:[NSArray class]]) {
                              return [value valueForKeyPath:@"@sum.holders"];
                          }
                          return @0;
                      }];
    [NSValueTransformer registerValueTransformerWithName:@"PubliBikeCity_StatusAvailable_Transformer" transformedValueClass:[NSNumber class]
                      returningTransformedValueWithBlock:^NSNumber*(NSArray* value) {
                          if([value isKindOfClass:[NSArray class]]) {
                              return [value valueForKeyPath:@"@sum.available"];
                          }
                          return @0;
                      }];
    [NSValueTransformer registerValueTransformerWithName:@"PubliBikeCity_Open_Transformer" transformedValueClass:[NSNumber class]
                      returningTransformedValueWithBlock:^NSNumber*(NSString* value) {
                          if([value isKindOfClass:[NSString class]]) {
                              return @([value isEqualToString:@"1"]);
                          }
                          return @YES;
                      }];
}

- (NSArray *) updateURLStrings { return @[@"http://customers2011.ssmservice.ch/publibike/getterminals_v2.php"]; }

- (NSString*) keyPathToStationsLists { return @"terminals"; }


- (NSDictionary *)KVCMappingDictionary
{
    return @{
             @"terminalid": @"number",
             @"lat": @"latitude",
             @"lng": @"longitude",
             @"street": @"address",
             @"name": @"name",
             @"bikeholders": @[@"PubliBikeCity_StatusFree_Transformer:status_free",
                               @"PubliBikeCity_StatusTotal_Transformer:status_total"],
             @"bikes": @"PubliBikeCity_StatusAvailable_Transformer:status_available",
             @"status": @"PubliBikeCity_Open_Transformer:open"
             };
}

- (void) insertStationWithAttributes:(NSDictionary*)stationAttributes
{
    if([stationAttributes[@"groupids"] containsObject:self.serviceInfo[@"publibike_groupid"]]) {
        [super insertStationWithAttributes:stationAttributes];
    }
}

@end
