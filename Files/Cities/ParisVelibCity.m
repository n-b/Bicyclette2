#import "JCDecauxCity.h"

#import "DebugLog.h"
#import "NSStringAdditions.h"

@interface ParisVelibCity : JCDecauxCity
@end

@interface NSString (ParisTweaks)
- (NSString*) stringWithEndOfAddress;
@end

@implementation ParisVelibCity

#pragma mark Annotations

- (NSString *) titleForStation:(Station*)station
{
    NSString * title = [super titleForStation:station];

    // remove city name
    NSRange endRange = [title rangeOfString:@"("];
    if(endRange.location!=NSNotFound)
        title = [title substringToIndex:endRange.location];
    
    title = [title stringByTrimmingWhitespace];
    
    return title;
}

@end

@implementation NSString (ParisTweaks)

- (NSString*) stringWithEndOfAddress
{
    NSArray * components = [self componentsSeparatedByString:@" - "];
    if([components count]>1) {
        return [components lastObject];
    }
    static NSRegularExpression * exp;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        exp = [NSRegularExpression regularExpressionWithPattern:@"[0-9]{5}" options:0 error:NULL];
    });
    NSTextCheckingResult * match = [exp firstMatchInString:self options:0 range:NSMakeRange(0, [self length])];
    if(match) {
        return [self substringFromIndex:[match range].location];
    } else {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"BicycletteLogParsingDetails"])
            DebugLog(@"Note : End of address not found = %@",self);
        return @"";
    }
}

@end
