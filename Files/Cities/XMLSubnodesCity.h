#import "BicycletteCity+Update.h"

@interface XMLSubnodesCity : BicycletteCity
- (void) parseData:(NSData*)data;
- (NSString*) stationElementName; // override if necessary
@end
