#import "XMLAttributesCity.h"
#import "NSStringAdditions.h"

@interface NextBikeCity : XMLAttributesCity
@end

@implementation NextBikeCity

#pragma mark City Data Update

- (NSString*) baseURL
{
    return @"http://nextbike.net/maps/nextbike-official.xml?city=";
}

- (NSArray *) updateURLStrings
{
    if(self.serviceInfo[@"update_url"]) {
        return [super updateURLStrings];
    }
    if( self.serviceInfo[@"regions"] )
    {
        NSDictionary * regions = self.serviceInfo[@"regions"];
        NSMutableArray * result = [NSMutableArray new];
        for (NSString * regionID in regions) {
            [result addObject:[self.baseURL stringByAppendingString:regionID]];
        }
        return result;
    }
    else if( self.serviceInfo[@"region"]) {
        return @[[self.baseURL stringByAppendingString:self.serviceInfo[@"region"]]];
    } else {
        return nil;
    }
}

- (NSDictionary *)KVCMappingDictionary
{
    return @{
             @"lng": @"longitude",
             @"lat": @"latitude",
             @"uid": @"number",
             @"name": @"name",
             @"bikes": @"status_available",
             @"bike_racks": @"status_total"
             };
}

- (BOOL) canShowFreeSlots
{
    return [self.serviceInfo[@"show_free_slots"] boolValue]; //default is NO
}

- (NSString *)stationElementName
{
    return @"place";
}

@end
