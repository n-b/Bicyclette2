#import "XMLSubnodesCity.h"

@interface BIXICity : XMLSubnodesCity
@end

@implementation BIXICity

- (NSDictionary *)KVCMappingDictionary
{
    return @{@"id": @"number",
             @"lat": @"latitude",
             @"long": @"longitude",
             @"name": @"name",
             @"nbEmptyDocks": @"status_free",
             @"nbBikes": @"status_available"
             };
}

- (NSString *)stationElementName
{
    return @"station";
}

@end
