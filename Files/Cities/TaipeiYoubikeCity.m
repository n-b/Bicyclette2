
#import "FlatListCity.h"

@interface TaipeiYoubikeCity : FlatListCity <CityWithFlatListOfStations>
@end

@implementation TaipeiYoubikeCity

- (NSArray *)updateURLStrings
{
    return @[@"http://its.taipei.gov.tw/atis_index/aspx/Youbike.aspx?Mode=1"];
}

- (NSArray*) stationAttributesArraysFromData:(NSData*)data;
{
    NSMutableArray * attributesArray = [NSMutableArray new];
    NSScanner * scanner = [NSScanner scannerWithString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    NSString * stationData;
    while ([scanner scanUpToString:@"|" intoString:&stationData]) {
        NSArray * attributes = [stationData componentsSeparatedByString:@"_"];
        [attributesArray addObject:attributes];
        [scanner scanString:@"|" intoString:nil];
    }
    return attributesArray;
}

- (NSDictionary *)KVCMappingDictionary
{
    return @{
             @0: @"number",
             @1: @"name",
             @2: @"status_total",
             @3: @"status_available",
             // @4: @"unused",
             @5: @"latitude",
             @6: @"longitude",
             @7: @"address",
             // @8: @"unused,
             @9: @"fullAddress"
             };
}

@end
