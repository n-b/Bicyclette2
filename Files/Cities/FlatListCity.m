#import "FlatListCity.h"

// Allow me to call methods of subclasses
@interface FlatListCity (CityWithFlatListOfStations) <CityWithFlatListOfStations>
@end

@implementation FlatListCity

- (void) parseData:(NSData*)data
{
    id attributesArray = [self stationAttributesArraysFromData:data];
    
    // Loop on attribute dictionaries
    for (NSDictionary * attributeDict in attributesArray) {

        // If a station dictionary attribute is a dictionary, flatten its keys in the station dictionary
        NSMutableDictionary * flattenAttributes = [attributeDict mutableCopy];
        if([attributeDict isKindOfClass:[NSDictionary class]]) {
            for (NSString * key in attributeDict) {
                NSDictionary * attribute = attributeDict[key];
                if([attribute isKindOfClass:[NSDictionary class]])
                {
                    for (id key2 in attribute) {
                        NSMutableString * flatkey = [key mutableCopy];
                        [flatkey appendString:@"."];
                        [flatkey appendString:key2];
                        flattenAttributes[flatkey] = attribute[key2];
                    }
                }
            }
        }

        // Go
        [self insertStationWithAttributes:flattenAttributes];
    }
}

@end
