#import "NSStringAdditions.h"
#import "CollectionsAdditions.h"
#import "JCDecauxCity.h"

@interface LyonVelovCity : JCDecauxCity
@end

@implementation LyonVelovCity

#pragma mark Annotations

- (NSString*) titleForStation:(Station*)station
{
    NSString * title = station.name;
    title = [title stringByTrimmingZeros];
    title = [title stringByDeletingPrefix:station.number];
    title = [title stringByTrimmingWhitespace];
    title = [title stringByDeletingPrefix:@"-"];
    title = [title stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    title = [title stringByTrimmingWhitespace];
    title = [title capitalizedStringWithCurrentLocale];
    return title;
}

#pragma mark City Data Update

- (NSDictionary*) zips
{
    return @{@"01": @"1",
             @"02": @"2",
             @"03": @"3",
             @"04": @"4",
             @"05": @"5",
             @"06": @"6",
             @"07": @"7",
             @"08": @"8",
             @"09": @"9",

             @"10": @"Villeurbanne",

             @"11": @"4",			 // 2 stations in Caluire-et-Cuire -> group with 4th
             @"12": @"Villeurbanne", // 2 stations in Vaulx-en-Velin -> group with Villeurbanne
             };
}

@end
