#import "CitiesController.h"

@class BicycletteCity;
@class Station;

@interface MapVC : UIViewController  <MKMapViewDelegate, CitiesControllerDelegate>
+ (instancetype) mapVCWithController:(CitiesController*)controller_;
@property (readonly,  nonatomic) CitiesController * controller;

@property (readonly) BOOL rendering;
@property BOOL stickyTitleMode;
@end

typedef NS_ENUM(NSInteger, StationAnnotationMode)
{
    StationAnnotationModeBikes,
    StationAnnotationModeParking
};

