@import UIKit;

@interface UIBarButtonItem (BICMargins)
+ (instancetype) bic_marginItemWithWidth:(CGFloat)width_;
+ (instancetype) bic_flexibleMarginButtonItem;
@end
