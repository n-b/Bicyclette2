@import Foundation;

@protocol DataUpdaterDelegate;

// Data Update generic machinery
@interface DataUpdater : NSObject

- (id) initWithURLStrings:(NSArray*)urlStrings delegate:(id<DataUpdaterDelegate>)delegate;
@property (nonatomic, weak) id<DataUpdaterDelegate> delegate;

- (void) cancel;

@end

/****************************************************************************/
#pragma mark -

@protocol DataUpdaterDelegate <NSObject>
@optional
- (NSURLRequest*) updater:(DataUpdater*)updater requestForURLString:(NSString *)urlString;
@required
- (void) updater:(DataUpdater*)updater didFailWithError:(NSError*)error;
- (void) updaterDidFinishWithNoNewData:(DataUpdater*)updater;
- (void) updater:(DataUpdater*)updater finishedWithNewDataChunks:(NSDictionary*)datas;
@end
