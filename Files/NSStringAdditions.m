#import "NSStringAdditions.h"


@implementation NSString (NSStringAdditions)


- (NSString*) stringByDeletingPrefix:(NSString*) prefix
{
	if( ![self hasPrefix:prefix] )
		return self;
	return [self substringFromIndex:[prefix length]];
}

- (NSString*) stringByTrimmingZeros
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"0"]];
}

- (NSString*) stringByTrimmingWhitespace
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSString*) capitalizedStringWithCurrentLocale
{
    return [self capitalizedStringWithLocale:[NSLocale currentLocale]];
}

@end
