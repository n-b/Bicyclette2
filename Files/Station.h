@import Foundation;
@import CoreData;
@import CoreLocation;
#import "LocalUpdateQueue.h"


@interface Station : NSManagedObject < Locatable, MKAnnotation>

// Meta info
+ (NSString*) entityName;
typedef struct {
    __unsafe_unretained NSString *address;
    __unsafe_unretained NSString *bonus;
    __unsafe_unretained NSString *color;
    __unsafe_unretained NSString *fullAddress;
    __unsafe_unretained NSString *latitude;
    __unsafe_unretained NSString *longitude;
    __unsafe_unretained NSString *name;
    __unsafe_unretained NSString *number;
    __unsafe_unretained NSString *open;
    __unsafe_unretained NSString *starred;
    __unsafe_unretained NSString *status_available;
    __unsafe_unretained NSString *status_date;
    __unsafe_unretained NSString *status_free;
    __unsafe_unretained NSString *status_ticket;
    __unsafe_unretained NSString *status_total;
} StationAttributes;
+  (StationAttributes) attributes;


// Attributes
@property (nonatomic, retain) NSString * number;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * fullAddress;

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;

@property (nonatomic) BOOL open;
@property (nonatomic) BOOL bonus;

@property (nonatomic) BOOL starred;

@property (nonatomic) NSDate * status_date;
@property (nonatomic) int16_t status_available;
@property (nonatomic) int16_t status_free;
@property (nonatomic) int16_t status_total;
@property (nonatomic) BOOL status_ticket;


// Locatable, MKAnnotation
- (CLLocation *) location;

@end

