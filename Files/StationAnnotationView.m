#import "StationAnnotationView.h"
#import "Station.h"
#import "Station+Update.h"
#import "StationAnnotationView+Drawing.h"

@interface StarButton : UIButton
@end

#define kImageMargins (CGSizeMake(10,10))

@implementation StarButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setImage:[[UIImage imageNamed:@"Star-off"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [self setImage:[[UIImage imageNamed:@"Star-on"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
        [self setImage:[[UIImage imageNamed:@"Star-on"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateHighlighted];
        self.frame = CGRectMake(0, 0, [UIImage imageNamed:@"Star-off"].size.width + 16, 100); // more than the callout height, including the pointer.
        self.backgroundColor = kBicycletteBlue;
        self.tintColor = [UIColor whiteColor];
        self.adjustsImageWhenHighlighted = NO;
    }
    return self;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return (CGRect){CGPointMake(8,12),[UIImage imageNamed:@"Star-off"].size};
}

@end

@implementation StationAnnotationView
{
    StarButton * _starButton;
}

- (id) initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if(nil!=self) {
        self.frame = CGRectMake(0,0,kStationAnnotationViewSize,kStationAnnotationViewSize);
        
        // Setup callout
        self.canShowCallout = YES;
        _starButton = [StarButton new];
        _starButton.selected = self.station.starred;
        self.leftCalloutAccessoryView = _starButton;
        
        _starButton.accessibilityLabel = NSLocalizedString(@"station.favorite", nil);
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stationDidBecomeStale:) name:StationNotifications.didBecomStale object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (Station*) station
{
    return (Station*)self.annotation;
}

- (void) setAnnotation:(id<MKAnnotation>)annotation
{
    for (NSString * property in [[self class] stationObservedProperties]) {
        [self.station removeObserver:self forKeyPath:property];
    }
    
    [super setAnnotation:annotation];
    
    for (NSString * property in [[self class] stationObservedProperties]) {
        [self.station addObserver:self forKeyPath:property options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:__FILE__];
    }
    
    [self setNeedsDisplay];
}

- (void) prepareForReuse
{
    [super prepareForReuse];
    for (NSString * property in [[self class] stationObservedProperties]) {
        [self.station removeObserver:self forKeyPath:property];
    }
    self.annotation = nil;
}

- (void) setMode:(StationAnnotationMode)mode_
{
    _mode = mode_;
    [self setNeedsDisplay];
}

- (void) setOnlyDisplayFavorites:(BOOL)onlyDisplayFavorites_
{
    _onlyDisplayFavorites = onlyDisplayFavorites_;
    self.layer.opacity = (self.station.starred || self.onlyDisplayFavorites==NO) ? 1 : 0.5;
}

/****************************************************************************/
#pragma mark KVO

+ (NSArray*) stationObservedProperties
{
    return @[ Station.attributes.status_available, Station.attributes.status_free, Station.attributes.starred ];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == __FILE__)
    {
        if([keyPath isEqualToString:Station.attributes.starred]) {
            _starButton.selected = self.station.starred;
        }
        
        [self setNeedsDisplay];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void) stationDidBecomeStale:(NSNotification*)note
{
    if(note.object==self.station || note.object==nil) {
        [self setNeedsDisplay];
    }
}

/****************************************************************************/
#pragma mark Display

- (void) displayLayer:(CALayer *)layer
{
    // Prepare Value
    UIColor * baseColor;
    NSString * text;

    if(self.station.status_date.bic_isFresh && [self station].open)
    {
        int16_t value;
        if(self.mode==StationAnnotationModeBikes)
            value = [self station].status_available;
        else
            value = [self station].status_free;
        
        if(value==0) baseColor = kCriticalValueColor;
        else if(value<=[[NSUserDefaults standardUserDefaults] integerForKey:@"StationStatus.warningValue"]) baseColor = kWarningValueColor;
        else baseColor = kGoodValueColor;
        
        text = [NSString stringWithFormat:@"%d",value];
    }
    else
    {
        baseColor = kUnknownValueColor;
        if([self station].status_total!=0)
            text = [NSString stringWithFormat:@"%d",[self station].status_total];
        else
            text = @"-";
    }
    
    self.layer.contents = (id)[StationAnnotationView sharedImageWithMode:self.mode
                                                         backgroundColor:baseColor
                                                                 starred:self.station.starred
                                                                   value:text];
    self.layer.opacity = (self.station.starred || self.onlyDisplayFavorites==NO) ? 1 : 0.2;
}

- (void) drawRect:(CGRect)rect
{
    
}

- (NSString *)accessibilityLabel
{
    return [self.annotation title];
}

@end
