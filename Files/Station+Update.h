#import "Station.h"

#pragma mark -
@interface Station (Update) <LocalUpdatePoint>
@end

extern const struct StationNotifications {
    __unsafe_unretained NSString * didBecomStale;
} StationNotifications;

@interface NSDate (BICStaleness)
- (BOOL) bic_isFresh;
@end
