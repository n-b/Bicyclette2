#import "CitiesController.h"
#import "BicycletteCity+Update.h"
#import "CollectionsAdditions.h"
#import "LocalUpdateQueue.h"
#import "GeofencesMonitor.h"
#import "StationsStatusSummary.h"
#import "ENGoogleMeasurementProtocol.h"

#pragma mark - Private Methods

@interface CitiesController () <CLLocationManagerDelegate, LocalUpdateQueueDelegate, GeofencesMonitorDelegate>
@property GeofencesMonitor * fencesMonitor;
@property LocalUpdateQueue * updateQueue;

@property MKCoordinateRegion viewRegion;

@property StationsStatusSummary * summary;
@end

#pragma mark -

@implementation CitiesController

- (id)init
{
    self = [super init];
    if (self) {
        // Create cities
        BicycletteCitySetSaveStationsWithNoIndividualStatonUpdates(YES);
        BicycletteCitySetStoresDirectory([NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]);
        self.cities = [BicycletteCity allCities];

        self.fencesMonitor = [[GeofencesMonitor alloc] initWithDelegate:self];
        self.updateQueue = [LocalUpdateQueue new];
        self.updateQueue.delegate = self;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityUpdated:)
                                                     name:BicycletteCityNotifications.updateSucceeded object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appStateChanged:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appStateChanged:) name:UIApplicationDidEnterBackgroundNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(monitoringEnabledDidChange)
                                                     name:GeofencesMonitorNotifications.monitoringEnabledDidChange object:nil];
    }
    return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) monitoringEnabledDidChange
{
    // Region monitoring was switched on/off
    [self addAndRemoveMapAnnotationsWithDataUpdated:YES];
}

/****************************************************************************/
#pragma mark -

- (BicycletteCity*) cityNamed:(NSString*)cityName
{
    return [self.cities firstObjectWithValue:cityName forKeyPath:@"cityName"];
}

- (void) setCurrentCity:(BicycletteCity *)currentCity_
{
    if(_currentCity != currentCity_)
    {
        _currentCity = currentCity_;
        self.updateQueue.city = _currentCity;
        
        if([_currentCity canUpdateIndividualStations]) {
            // If the city update stations one by one,
            // build a cache of stations status now.
            [self.currentCity cacheStationsStatus];
        }

        [self analytics_tagCity];
    }
}

- (CLLocationDistance) regionSpanMeters
{
    CLLocation * northLocation = [[CLLocation alloc] initWithLatitude:self.viewRegion.center.latitude+self.viewRegion.span.latitudeDelta/2 longitude:self.viewRegion.center.longitude];
    CLLocation * southLocation = [[CLLocation alloc] initWithLatitude:self.viewRegion.center.latitude-self.viewRegion.span.latitudeDelta/2 longitude:self.viewRegion.center.longitude];
    CLLocation * westLocation = [[CLLocation alloc] initWithLatitude:self.viewRegion.center.latitude longitude:self.viewRegion.center.latitude-self.viewRegion.span.longitudeDelta/2];
    CLLocation * eastLocation = [[CLLocation alloc] initWithLatitude:self.viewRegion.center.latitude longitude:self.viewRegion.center.latitude+self.viewRegion.span.longitudeDelta/2];
    CLLocationDistance viewSpanMeters = ([northLocation distanceFromLocation:southLocation]+[eastLocation distanceFromLocation:westLocation])/2; // average
    return viewSpanMeters;
}

- (void) regionDidChange:(MKCoordinateRegion)viewRegion
{
    self.viewRegion = viewRegion;
    
    // center & span
    CLLocation * center = [[CLLocation alloc] initWithLatitude:self.viewRegion.center.latitude longitude:self.viewRegion.center.longitude];
    CLLocationDistance viewSpanMeters = [self regionSpanMeters];

    // Get the nearest city
    BicycletteCity * nearestCity = nil;
    nearestCity = (BicycletteCity*)[self.cities nearestLocatableFrom:center];
    CLLocationDistance distanceToCity = [nearestCity.location distanceFromLocation:center];
    
    // Are we actually near from the city ?
    CLLocationDistance minRadius = [[NSUserDefaults standardUserDefaults] doubleForKey:@"Cities.MinimumRadius"];
    CLLocationDistance effectiveRadius = MAX([nearestCity radius], minRadius);
    if(distanceToCity > effectiveRadius * [[nearestCity prefForKey:@"CitiesController.CurrentCityDistanceThreshold"] doubleValue])
        nearestCity = nil;

    // Are we zooming enough ?
    if(viewSpanMeters > effectiveRadius * [[nearestCity prefForKey:@"CitiesController.CurrentCityZoomThreshold"] doubleValue])
        nearestCity = nil;
    
    BOOL dataUpdated = NO;
    if(self.currentCity!=nearestCity) {
        self.currentCity = nearestCity;
        dataUpdated = YES;
    }

    // Update annotations
    [self addAndRemoveMapAnnotationsWithDataUpdated:dataUpdated];

    self.updateQueue.visibleRegion = self.viewRegion;
}

- (void) addAndRemoveMapAnnotationsWithDataUpdated:(BOOL)dataUpdated
{
    NSMutableArray * newAnnotations = [NSMutableArray new];
    NSMutableArray * newOverlays = [NSMutableArray new];
    
    if (self.currentCity==nil) {
        // World Cities
        NSArray * cities = self.cities;
        [newAnnotations addObjectsFromArray:cities];
    } else {
        // Fences
        if([self.fencesMonitor isMonitoringEnabled]) {
            NSArray * fences = [self.fencesMonitor geofencesInCity:_currentCity];
            [newOverlays addObjectsFromArray:fences];
        }

        // Stations
        double cityArea = M_PI*self.currentCity.radius*self.currentCity.radius;
        CLLocationDistance viewSpanMeters = [self regionSpanMeters];
        double visibleArea = viewSpanMeters * viewSpanMeters;
        CGFloat cityNumberOfStations = [self.currentCity numberOfStations];
        CGFloat visibleStationsEstimate = cityNumberOfStations/cityArea*visibleArea;
        // ViewSpanMeters is an average, so visibleArea is underestimating.
        // Additionally, cityArea tends to be larger than what is seems, because of density is inequal.
        // Let's add some magic factor.
        visibleStationsEstimate *= 2;
        visibleStationsEstimate = MIN(visibleStationsEstimate, cityNumberOfStations);
        double threshold;
        if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPad) {
            threshold = [[NSUserDefaults standardUserDefaults] doubleForKey:@"CitiesController.StationsZoomThreshold~iPad"];
        } else {
            threshold = [[NSUserDefaults standardUserDefaults] doubleForKey:@"CitiesController.StationsZoomThreshold~iPhone"];
        }
        BOOL shouldShowOverlay = visibleStationsEstimate > threshold;

        if(shouldShowOverlay) {
            [newOverlays addObjectsFromArray:@[self.currentCity]];
        } else {
            MKCoordinateRegion mapRegion = self.viewRegion;
            mapRegion.span.latitudeDelta *= 1.25; // Add stations that are just off screen limits
            mapRegion.span.longitudeDelta *= 1.25;
            [newAnnotations addObjectsFromArray:[self.currentCity stationsWithinRegion:mapRegion]];
        }
    }

    [self.delegate controller:self setAnnotations:newAnnotations overlays:newOverlays dataUpdated:dataUpdated];
}


- (void) selectCity:(BicycletteCity*)city_
{
    MKCoordinateRegion region = [city_ mkRegionContainingData];
    if(region.span.latitudeDelta == 0. || region.span.longitudeDelta == 0.) {
        CLLocationDistance minDistance = [[NSUserDefaults standardUserDefaults] doubleForKey:@"CitiesController.MapRegionZoomDistance"];
        region = MKCoordinateRegionMakeWithDistance(region.center, minDistance, minDistance);
    }
    [self.delegate controller:self setRegion:region];
}

- (void) switchStarredStation:(Station*)station
{
    // Make the change in the main context, so that it's reflected immediately in the UI
    station.starred = !station.starred;

    [station.city performUpdates:^(NSManagedObjectContext *updateContext) {
        Station * lstation = (Station*)[updateContext objectWithID:[station objectID]];
        lstation.starred = !lstation.starred;
    } saveCompletion:^(NSNotification *contextDidSaveNotification) {
        NSArray * starredStations = [station.city starredStations];
        [self.fencesMonitor setStarredStations:starredStations inCity:station.city];
        [StationsStatusSummary requestNotificationAuthorization];
        self.updateQueue.starredStations = _currentCity.starredStations;
        [self addAndRemoveMapAnnotationsWithDataUpdated:YES];
    }];
}

/****************************************************************************/
#pragma mark -

- (void) appStateChanged:(NSNotification*)note
{
    BOOL background = [note.name isEqualToString:UIApplicationDidEnterBackgroundNotification];
    self.updateQueue.paused = (background || self.mapViewIsMoving);
}

- (void) setMapViewIsMoving:(BOOL)moving
{
    _mapViewIsMoving = moving;
    BOOL background = [[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground;
    self.updateQueue.paused = (background || self.mapViewIsMoving);
}

/****************************************************************************/
#pragma mark -

- (void)monitor:(GeofencesMonitor *)monitor fenceMonitoringFailed:(Geofence *)fence withError:(NSError *)error
{
    NSArray * objectIDs = [fence.stations valueForKeyPath:@"objectID"];
    [fence.city performUpdates:^(NSManagedObjectContext *updateContext) {
        for (NSManagedObjectID * stationID in objectIDs) {
            Station * station = (Station *)[updateContext objectWithID:stationID];
            station.starred = NO;
        }
    } saveCompletion:^(NSNotification *contextDidSaveNotification) {
        NSString * message = [NSString stringWithFormat:@"%@\n%@",
                              NSLocalizedString(@"geofencing.error.message",nil),
                              [error localizedDescription]];
        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"geofencing.error.title",nil)
                                   message:message
                                  delegate:nil
                         cancelButtonTitle:NSLocalizedString(@"geofencing.error.ok",nil)
                         otherButtonTitles:nil]
         show];
        NSArray * starredStations = [fence.city starredStations];
        [self.fencesMonitor setStarredStations:starredStations inCity:fence.city];
        [self addAndRemoveMapAnnotationsWithDataUpdated:NO];
    }];
}

- (void) monitor:(GeofencesMonitor*)monitor fenceWasEntered:(Geofence*)fence
{
    if(self.summary) {
        [self.summary cancel];
    }

    self.summary = [[StationsStatusSummary alloc] initWithFences:[self.fencesMonitor geofencesInCity:fence.city] relativeToFence:fence entered:YES];
    self.updateQueue.summary = self.summary;
}

- (void) monitor:(GeofencesMonitor*)monitor fenceWasExited:(Geofence*)fence
{
    if(self.summary) {
        [self.summary cancel];
    }

    self.summary = [[StationsStatusSummary alloc] initWithFences:[self.fencesMonitor geofencesInCity:fence.city] relativeToFence:fence entered:NO];
    self.updateQueue.summary = self.summary;
}

- (BicycletteCity*) monitor:(GeofencesMonitor*)monitor cityWithRestoredName:(NSString*)cityName
{
    return [self cityNamed:cityName];
}

/****************************************************************************/
#pragma mark -

- (void) updateQueueDidComplete:(LocalUpdateQueue *)queue updatedPoints:(NSArray*)points errors:(NSArray*)errors
{
    if([errors count]) {
        [self.summary cancel];
    } else {
        [self.summary presentSummary];
    }
    self.updateQueue.summary = nil;
    self.summary = nil;

    if([self.currentCity canUpdateIndividualStations]) {
        // If the current city updates stations individually,
        // we need to refresh its stations status cache.
        [self.currentCity cacheStationsStatus];
        [self addAndRemoveMapAnnotationsWithDataUpdated:YES];
    }
}

/****************************************************************************/
#pragma mark -

- (void) cityUpdated:(NSNotification*) note
{
    if([note.userInfo[BicycletteCityNotifications.keys.dataChanged] boolValue]){
        [self addAndRemoveMapAnnotationsWithDataUpdated:YES];
    }
}

/****************************************************************************/
#pragma mark -

- (void) handleLocalNotificaion:(UILocalNotification*)notification
{
    [self selectStationNumber:notification.userInfo[@"stationNumber"] inCityNamed:notification.userInfo[@"city"] changeRegion:YES];
}

- (void) selectStationNumber:(NSString*)stationNumber inCityNamed:(NSString*)cityName changeRegion:(BOOL)changeRegion
{
    BicycletteCity * city = [self cityNamed:cityName];
    
    Station * station = nil;
    if(stationNumber)
    {
        station = [city stationWithNumber:stationNumber];
    }
    
    if(city && stationNumber)
    {
        self.currentCity = city;
        if(changeRegion)
        {
            CLLocationDistance meters = [[city prefForKey:@"CitiesController.MapRegionZoomDistance"] doubleValue];
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(station.coordinate, meters, meters);
            [self.delegate controller:self setRegion:region];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // do it after a delay to make sure the station is visible
            [self.delegate controller:self selectAnnotation:station];
        });
    }
}

#pragma mark - Google Analytics

- (void) analytics_tagCity
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"DisableGAI"]) {
        if(_currentCity) {
            [[ENGAManager sharedManager] event:@{@(kEventCategoryKey): @"citychange",
                                                 @(kEventActionKey): _currentCity.cityName
                                                 }];
        }
    }
}

@end
