#import "MapVC.h"

@interface CityOverlayRenderer : MKOverlayRenderer
@property (nonatomic) StationAnnotationMode mode;
@property (nonatomic) BOOL onlyDisplayFavorites;
@end
